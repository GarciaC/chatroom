Template.home.onCreated(function () {
    Meteor.subscribe('posts');
});

Template.home.events({
    "submit .add-new-post": function (event) {
        event.preventDefault();
        var name = event.target.firstname.value;

        Posts.insert({
            name: name,
            username: Meteor.user().username,
            createdAt: new Date(),
        });
    }
});

Template.post.events({
   "click .post-class": function () { 
       Router.go('homescreen');
   }
});