Router.route('/', function () {
    this.layout('ApplicationLayout');
    this.render('home');
}, {
    name: "homescreen"
});

Router.route('/post/:id', function () {
    this.layout('ApplicationLayout');
    this.render('post', {
        data: function () {
            return Posts.findOne({
                _id: this.params._id
            });
        }
    });
});